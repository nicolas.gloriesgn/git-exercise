# Évaluation Git

- Prénom et nom : Nicolas Glories, Sibille Lazraoui
- Commentaires : .........................

## Exercices

- 1 point - Fourcher et cloner le dépôt
git clone https://gitlab.com/nicolas.gloriesgn/git-exercise.git

- 1 point - Remplir votre nom ci-dessus 

- 1 point - Reprendre dans master le commit de la branche `equality_refinement` via `cherry-pick`
git cherry-pick fbca3b123bb5735d83f9b9efa8c415024bf4edd8

- 1 point - Défaire le commit `remove unused subtract functions`
    git revert 018773291b6c6a6463f2c208209c55551e5c8912

- 1 point - Fusionner dans `master` la branche `doc/intro`
git add -Av
git commit -m "readme"
git merge doc/intro

- 2 points - Fusionner dans `master` la branche `add_distance`
git merge origin/add_distance

- 1 point - Pousser le nouveau `master`
git push origin master


- 2 points - Créer une branche sur le dernier `master`, créer une nouvelle fonction de calcul, la committer, et pousser cette nouvelle branche 
git branch function
 git add -Av
git commit -m "add function test"
git push origin master

- 2 points - Rebaser la branche `add_tests` sur `master`
	git checkout add_tests
	git rebase origin/master
- 1 point - Pousser la branche `add_tests` sur le dépôt distant
- 2 points - Ouvrir une merge request à partir de cette branche `add_tests` sur le dépôt source


## Questions

- Donner l’auteur du commit qui a introduit la fonction `add` dans ce projet : ....................................................

- Indiquer quelle commande utiliser, dans un projet Git,  pour changer le message du dernier commit .........................................................

- Quelle commande, inverse de `git add`, permet de retirer un fichier du stage (prochain commit) ? ...........................................................


- Donner le nom du créateur de Git : .............................................

- Indiquer la(les) différence(s) entre les commandes `git init` et `git clone` : ................................................
